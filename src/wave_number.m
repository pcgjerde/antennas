function [k0,k] = wave_number(lambda,epsilon_r)
% This functions finds the complex wave number given wavelength and
% permittivity
%   lambda = wavelength
%   epsilon_r = permitivity

k0 = (2*pi)/(lambda);
k = k0*sqrt(epsilon_r);
end

