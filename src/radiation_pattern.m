function [Etheta,Ephi] = radiation_pattern(theta,phi,width,length,V,t,k0,k)
% Simulation of E_theat and E_phi for a rectangular patch antenna
%
% Based on work from "Microstrip Antenna Technology," by K.R. Carver
% and J. W. Mink in Microstrip Antennas (D.M. Pozar and D. H. Schaubert,
% eds).
%
%   theta = theta value
%   phi = phi value
%   lambda = wavelength
%   width = width of the patch
%   length = length of the patch
%   k0 = wave number
%   k = complex wave number

% Leading term
V = (1j*V*k0*length*exp(-1j*k0));
% The imaging factor accounts for reflections from the ground plane
image_factor = cos(k*t*cos(theta));
% solution for maxwells equations
M = (sin(k0.*(length/2).*sin(theta).*sin(phi))./(k0.*(length/2).*sin(theta).*sin(phi))).*(cos(k0.*(width/2)).*sin(theta));
% the E fields
Etheta = -V.*image_factor.*M.*cos(phi);
Ephi = -V.*image_factor.*M.*cos(theta).*sin(phi);

end