%E_pot - a short script that displays plots of the E field of a patch
%antenna along theta and phi slices

% Paul Gjerde <pcgjerde@mtu.edu>
% 17 Febuary, 2020

theta = linspace(0,2*pi,10000);
phi = linspace(pi/2,pi/2,10000);
c = physconst('LightSpeed');

f = 2.4E9;
lambda = (c)/(f);
epsilon_r = 4;
V = 1;
t = 0;

L = patch_length(f,epsilon_r);
W = patch_length(f,epsilon_r);

[k0,k] = wave_number(lambda,epsilon_r);

[Etheta,Ephi] = radiation_pattern(theta,phi,L,W,V,t,k0,k);

f1 = figure('Name','E_theta','NumberTitle','off');
polarplot(theta,Etheta,'.')

f2 = figure('Name','E_phi','NumberTitle','off');
polarplot(phi,Ephi,'.')