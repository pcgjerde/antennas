function [L] = patch_length(fc,epsilon_r)
%PATCH_FREQUENCY function to find length of patch antenna
%   fc       = center frequency
%   epsilonr = permitivity constant

c = physconst('LightSpeed');
L = 0.49*(c/(fc*sqrt(epsilon_r)));
end